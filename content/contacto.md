+++
title = "Contacto"
slug = "contacto"
showInMenu = true
hideLastModified = true
+++

Puedes contactar conmigo en la siguiente dirección de correo electrónico:

>> _fjaviermunoz **arroba** telefonica **punto** net_

Se muestra de esta forma para **evitar spam**.

También tengo un perfil en **LinkedIn**, accesible [aquí](https://www.linkedin.com/in/francisco-javier-muñoz-monge-7545241b6).
