+++
title = "Sobre mí"
slug = "sobremi"
showInMenu = true
hideLastModified = true
+++

![Javi](/images/javi.jpg)

Mi nombre es **Francisco Javier** y he trabajado como arquitecto **treinta y ocho años**.

Me jubilé en 2020 y aunque mi actividad profesional **ha cesado**, sigo escuchando debates relacionados con mi profesión.

**Estaré encantado** de charlar sobre los temas que más me interesan.

Puedes contactar conmigo a través de email [aquí](/contacto).
